import time
import detectron2
from detectron2.utils.logger import setup_logger

setup_logger()
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer, ColorMode
from detectron2.data import MetadataCatalog, DatasetCatalog
from detectron2.structures import Boxes
import torch
import numpy as np
import cv2
import os
import glob
import math
import csv
import warnings
warnings.filterwarnings("ignore")

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

cfg = get_cfg()
# Path to the model:
model = "detectron2/configs/COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml"
# Path to the weights:
weights = "detectron2/models/model_final_68b088.pkl"
# Load model:
model = cfg.merge_from_file(model)
# Load weights:
cfg.MODEL.WEIGHTS = weights
# Model confidence set at:
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7
# Backbone Device:
cfg.MODEL.DEVICE = "cuda"  # cpu
predictor = DefaultPredictor(cfg)


my_classes = ["person", "car", "truck", "bus", "motorcycle"]
c = 0

# Will create an "anno" folder if it is not exist, to save the detection coordinates in a txt file (for evalution purposes)
if os.path.isdir("detectron2/outputs/anno"):
    print("\n", "---------- anno-Folder Already Exists! -------------")
else:
    os.mkdir("detectron2/outputs/anno")
    print("\n", "---------- anno-Folder is Created! -------------")

def box_details(xmin, ymin, xmax, ymax, w_img, h_img):
    x_center = ((xmax + xmin) / 2) / w_img
    y_center = ((ymax + ymin) / 2) / h_img
    w_box = xmax - xmin
    h_box = ymax - ymin
    width = w_box / w_img
    height = h_box / h_img

    yolo_coor = [x_center, y_center, width, height]
    return yolo_coor


for path in sorted(glob.glob("detectron2/images/*")):
    img = cv2.imread(path)
    h_img, w_img = img.shape[:2]
    img_name = path.split("/")[-1].split(".")[0]
    exe = path.split("/")[-1].split(".")[1]
    # predictions and only cup detection display
    starttime = time.time()
    outputs = predictor(img)
    endtime = time.time()
    
    v = Visualizer(img[:, :, ::-1], MetadataCatalog.get(cfg.DATASETS.TRAIN[0]), scale=1.2)
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 0].to("cpu")) # person
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 2].to("cpu")) # car
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 3].to("cpu")) # motorcycle or motorbike
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 5].to("cpu"))  # bus
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 7].to("cpu")) # truck
    length = len(outputs["instances"].pred_boxes.tensor.cpu().numpy())
    if length == 0: # if the list of classes is empty
        with open("detectron2/outputs/anno/" + img_name + ".txt", 'a+') as f:


            f.write("")  # means do not write anything
        f.close()
        print("\n", "Sorry! I could not detect any object in the {} image. ".format(img_name), "\n")
        
    else:
        for i, label in enumerate(outputs["instances"].pred_classes):
            num = label.item()
            Class = MetadataCatalog.get(cfg.DATASETS.TRAIN[0]).thing_classes[num]
            if any(Class in s for s in my_classes):
                box = outputs["instances"].pred_boxes.tensor.cpu().numpy()[i]
                xmin = box[0]
                ymin = box[1]
                xmax = box[2]
                ymax = box[3]
                coor = box_details(xmin, ymin, xmax, ymax, w_img, h_img)
                conf = outputs["instances"].scores[i].item()
                conf = math.floor(conf * 10 ** 2) / 10 ** 2
                if num == 7:
                    num = 2
                with open("detectron2/outputs/anno/" + img_name + ".txt", 'a+') as f:
                    f.write("{} {} {} {} {} {}".format(num, conf, coor[0], coor[1], coor[2], coor[3]))
                    f.write("\n")
                f.close()


            else:
                with open("detectron2/outputs/anno/" + img_name + ".txt", 'a+') as f:


                    f.write("")  # means do not write anything
                f.close()

    cv2.imwrite("detectron2/outputs/" + img_name + "." + exe, out.get_image()[:, :, ::-1])
    sec = endtime - starttime
    sec = round(sec, 2)
  
    print("\n", "- {} has been processed successfully in {} sec!".format(img_name, sec), "\n")
    c = c + 1
    file_exists = os.path.isfile("report.csv")

    with open(r'report.csv', 'a', newline='') as f:
        headers = ["Image_name", "Time_inference (in sec)", "Img_H", "Img_W"]
        writer = csv.DictWriter(f, delimiter=',', lineterminator='\n', fieldnames=headers)
        if not file_exists:
            writer.writeheader()  # file doesn't exist yet, write a header

        writer.writerow({"Image_name" : img_name, "Time_inference (in sec)" : sec, "Img_H" : h_img , "Img_W" : w_img })

print("\n", "Total number of images processed successfully!: {}".format(c))

