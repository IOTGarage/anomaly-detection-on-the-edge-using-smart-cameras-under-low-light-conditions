import time
import detectron2
from detectron2.utils.logger import setup_logger

setup_logger()
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer, ColorMode
from detectron2.data import MetadataCatalog, DatasetCatalog
from detectron2.structures import Boxes
import os
import cv2
import math
import glob
import shutil
import warnings
warnings.filterwarnings("ignore")

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

cfg = get_cfg()
# Path to the model:
model = "detectron2/configs/COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x.yaml"
# Path to the weights:
weights = "detectron2/models/model_final_68b088.pkl"
# Load model:
model = cfg.merge_from_file(model)
# Load weights:
cfg.MODEL.WEIGHTS = weights
# Model confidence set at:
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7
# Backbone Device:
cfg.MODEL.DEVICE = "cuda"  # cpu
predictor = DefaultPredictor(cfg)


my_classes = ["person", "car", "truck", "bus", "motorcycle","bicycle","boat", "bottle","cat", "chair", "cup", "dog", "table"]
c = 0

os.mkdir("detectron2/outputs/anno")
print("\n---------- anno-Folder is Created! -------------\n")

def box_details(xmin, ymin, xmax, ymax, w_img, h_img):
    x_center = ((xmax + xmin) / 2) / w_img
    y_center = ((ymax + ymin) / 2) / h_img
    w_box = xmax - xmin
    h_box = ymax - ymin
    width = w_box / w_img
    height = h_box / h_img

    yolo_coor = [x_center, y_center, width, height]
    return yolo_coor

exe = ['.jpg', '.jpeg', '.png', '.PNG', '.JPEG', '.JPG']

for path in sorted(glob.glob("detectron2/images/*{}".format(exe))):
    img = cv2.imread(path)
    h_img, w_img = img.shape[:2]
    img_name = path.split("/")[-1].split(".")[0]
    exe = path.split("/")[-1].split(".")[1]
    # predictions and only cup detection display
    starttime = time.time()
    outputs = predictor(img)
    endtime = time.time()

    
    v = Visualizer(img[:, :, ::-1], MetadataCatalog.get(cfg.DATASETS.TRAIN[0]), scale=1.2)
    v.thing_colors = [(0, 255, 0)]  # set only one color for all things (green)
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 0].to("cpu")) # person
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 2].to("cpu")) # car
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 3].to("cpu")) # motorcycle or motorbike
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 5].to("cpu"))  # bus
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 7].to("cpu")) # truck
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 1].to("cpu")) # "bicycle"
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 8].to("cpu")) # "boat"
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 39].to("cpu")) # "bottle"
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 15].to("cpu")) # "cat"
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 56].to("cpu")) # "chair"
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 41].to("cpu")) # "cup"
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 16].to("cpu")) # "dog"
    out = v.draw_instance_predictions(outputs['instances'][outputs['instances'].pred_classes == 60].to("cpu"))  #"table"
    length = len(outputs["instances"].pred_boxes.tensor.cpu().numpy())
    if length == 0: # if the list of classes is empty
        with open("detectron2/outputs/anno/" + img_name + ".txt", 'a+') as f:

            f.write("")  # means do not write anything
        f.close()
        print("\n", "Sorry! I could not detect any object in the {} image. ".format(img_name), "\n")
        
    else:
        for i, label in enumerate(outputs["instances"].pred_classes):
            num = label.item()
            Class = MetadataCatalog.get(cfg.DATASETS.TRAIN[0]).thing_classes[num]
            #print(Class)
            if any(Class in s for s in my_classes):
                box = outputs["instances"].pred_boxes.tensor.cpu().numpy()[i]
                xmin = box[0]
                ymin = box[1]
                xmax = box[2]
                ymax = box[3]
                #print(xmin, ymin, xmax, ymax)
                coor = box_details(xmin, ymin, xmax, ymax, w_img, h_img)
                conf = outputs["instances"].scores[i].item()
                conf = math.floor(conf * 10 ** 2) / 10 ** 2
                if num == 7:
                    num = 2
                with open("detectron2/outputs/anno/" + img_name + ".txt", 'a+') as f:
                    #f.truncate(0)  # will clear the txt
                    f.write("{} {} {} {} {} {}".format(num, conf, coor[0], coor[1], coor[2], coor[3]))
                    f.write("\n")
                f.close()

            else:
                with open("detectron2/outputs/anno/" + img_name + ".txt", 'a+') as f:

                    f.write("")  # means do not write anything
                f.close()

    cv2.imwrite("detectron2/outputs/" + img_name + "." + exe, out.get_image()[:, :, ::-1])
    sec = endtime - starttime
    sec = round(sec, 2)
    print("\n", "- {} has been processed successfully in {} sec!".format(img_name, sec), "\n")
    c = c + 1

print("\n", "Total number of images processed successfully!: {}".format(c))



