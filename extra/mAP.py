import os
import cv2
import glob
import pandas as pd
from map_boxes import mean_average_precision_for_boxes
import shutil


dic = {0: "Person", 1:"Bicycle", 2: "Car", 3: "Motorcycle", 5:"Bus", 8:"Boat", 15:"Cat", 16:"Dog",
       39:"Bottle", 56: "Chair", 41: "Cup", 60: "Table"}

exe = ['.jpg', '.jpeg', '.png', '.PNG', '.JPEG', '.JPG']

# Path to the folders, each folder is an enhancement model, inside each folder enhanced images by that model
folder_path = ".\\<folder_with enhanced_images_per_model>\\*"
empty = 0

# Convert YOLO coordinates to COCO coordinates:
def yolo_coco(xc, yc, w, h, h_img, w_img):
    w_coco = int(w * w_img)
    h_coco = int(h * h_img)
    x_coco = int(xc * w_img - (w_coco/2))
    y_coco = int(yc * h_img - (h_coco/2))
    return int(x_coco), int(y_coco), int(w_coco), int(h_coco)

# =============================================================================
# Calculate mAP for all images:
# =============================================================================
for f in glob.glob(folder_path): 
    folder_name = f.split("\\")[-1]

    # In case a direct detection on the dark images (to compare between before and after enhancement)
    if folder_name == "Direct":
        continue
    print("\n################### {} #################".format(folder_name))
    df = pd.DataFrame(columns=["img_name", "mAP","Person", "Bicycle",  "Car",  "Motorcycle", "Bus", "Boat", "Cat", "Dog",
              "Bottle", "Chair", "Cup", "Table"])
    
    # path to the anno folder 
    for file in glob.glob(f + ".\\anno\\*"): 
        
        file_name = file.split("\\")[-1]
        path_to_anno = file
        # Use the file name of that prediction file to get the ground truth file
        path_to_gt = ".\\gt\\{}".format(file_name)
                    
        # Check if the file is empty is empty or not:
        filesize = os.path.getsize(file)
        if filesize == 0:
            pred = pd.DataFrame(data = [[0, 0, 0, 0, 0, 0]])
            empty = empty + 1 
            
        else:
            # Read the prediction in a txt file
            pred = pd.read_csv(file, header=None, sep=" ")                   
        
        # Read the ground truth file in a txt file
        gt = pd.read_csv(path_to_gt, header=None, sep=" ")   
        
        # To check if the image in the OD exists in the IE:
        imgs = [f for f in os.listdir(".\\{}\\OD\\".format(folder_name)) if f.endswith(tuple(exe))]            
        for im in imgs :
            if im.split(".")[0] == file_name.split(".")[0]:                    
                    
                path_to_img = ".\\{}\\IE\\{}".format(folder_name, im)
                image = cv2.imread(path_to_img)
                h_img, w_img = image.shape[:2]
    
                gts= []
                
                # Extract cooordinates from gt and sort them in a list: 
                for index, row in gt.iterrows():
                    classg = dic[row[0]]
                    xg = row[1]
                    yg = row[2]
                    wg = row[3]
                    hg = row[4]

                    # Convert to coco for gt:       
                    x1g, y1g, wg, hg  = yolo_coco(xg, yg, wg, hg, h_img, w_img)
                    x2g = x1g + wg
                    y2g = y1g + hg
                    g = [im, classg, x1g/w_img, x2g/w_img, y1g/h_img, y2g /h_img]
                    gts.append(g)
                 
                # Create a list for each class
                Person = [] 
                Bicycle = []
                Car = []
                Motorcycle = []
                Bus = []
                Boat = [] 
                Cat = []
                Dog = []
                Bottle = []
                Chair = []
                Cup = []
                Table = []
                    
                preds = []

                # Extract cooordinates from pred and sort them in a list:
                for index, row in pred.iterrows():
                    classp = dic[row[0]]
                    confp = row[1]
                    xp = row[2]
                    yp = row[3]
                    wp = row[4]
                    hp = row[5]
                    
                    x1p, y1p, wp, hp  = yolo_coco(xp, yp, wp, hp, h_img, w_img)
                    x2p = x1p + wp
                    y2p = y1p + hp
                    p = [im, classp, confp, x1p/w_img, x2p/w_img, y1p/h_img, y2p/h_img]
                    preds.append(p)
                    
                    Id = int(row[0])

                    if row[0] == 0:
                        Person.append(row[0])
                    elif row[0] == 1:
                        Bicycle.append(row[0])                    
                    elif row[0] == 2:
                        Car.append(row[0])                    
                    elif row[0] == 3:
                        Motorcycle.append(row[0])
                    elif row[0] == 5:
                        Bus.append(row[0])
                    elif row[0] == 8:
                        Boat.append(row[0])
                    elif row[0] == 15:
                        Cat.append(row[0])
                    elif row[0] == 16:
                        Dog.append(row[0])
                    elif row[0] == 39:
                        Bottle.append(row[0])
                    elif row[0] == 56:
                        Chair.append(row[0])
                    elif row[0] == 41:
                        Cup.append(row[0])
                    elif row[0] == 60:
                        Table.append(row[0])                       
                '''
                This is the main function to calculate the mAP of the object detection on a single image.
                '''
                mean_ap, average_precisions  = mean_average_precision_for_boxes(gts, preds)
    
               
                to_append = [im, mean_ap, len(Person), len(Bicycle), len(Car), len(Motorcycle), len(Bus)
                             , len(Boat), len(Cat), len(Dog), len(Bottle), len(Chair), len(Cup), len(Table)] 
                
                df_length = len(df)
                df.loc[df_length] = to_append

    df.to_csv('.\\{}.csv'.format(folder_name), index=None)
print("\n {} saved!".format(folder_name))           
                
# =============================================================================
# Read CSV files and move imgs with mAP > threshold
# =============================================================================

# Path to the csv files which contain the mAP per image and per model
file_csv = ".\\mAP\\*"

# The threshold for the image to be considered:
threshold = 0.9

# Number of csv files:
files_csv = len([f for f in os.listdir(".\\mAP") if f.endswith(".csv")])    

# Make a new dic to move images with >= threshold, each folder will represent a specific model
if not os.path.exists(".\\mAP\\imgs"):
    os.mkdir(".\\mAP\\imgs")
    
mAP_all_imgs = []

for csv in glob.glob(".\\mAP\\*.csv"):
    folder_name = csv.split("\\")[-1].split(".")[0]
    print("########## {} ###########".format(folder_name))
    os.mkdir(".\\mAP\\imgs\\{}".format(folder_name))
    
    # Read csv in a dataframe:
    df = pd.read_csv(csv)  
    
    # COPY images with mAP greater than threshold
    for i, r in df.iterrows():
        img_name = r[0]
        # To make sure the img corresponding exist in the original dataset.
        if os.path.exists(".\\ExDark_all\\{}".format(img_name)):
            mAP = r[1]
            if mAP >= threshold:
                mAP_all_imgs.append(img_name)
                # copy from original dataset for original dark pixels:
                source = ".\\ExDark_all\\{}".format(img_name)
                dest = ".\\mAP\\imgs\\{}\\{}".format(folder_name, img_name) 
                shutil.copy(source, dest) 
        else:
            continue
        


# =====================================================================================================================
# Check similar images in imgs directory and move them to separate ones
# =====================================================================================================================

# Create a new dic for each model inside the root dic "imgs_dup"
os.mkdir(".\\mAP\\imgs_dup\\CSDNet_UPE")
os.mkdir(".\\mAP\\imgs_dup\\LiteCSDNet_UPE")
os.mkdir(".\\mAP\\imgs_dup\\RUAS")
os.mkdir(".\\mAP\\imgs_dup\\SLiteCSDNet_UPE")
os.mkdir(".\\mAP\\imgs_dup\\LiteCSDNet_LOL")
os.mkdir(".\\mAP\\imgs_dup\\Zero-DCE++")
################################# This suppose to be run once

# Check the number of imgs inside each folder for each model:
CSDNet_UPE = os.listdir(".\\mAP\\imgs\\CSDNet_UPE")
LiteCSDNet_UPE = os.listdir(".\\mAP\\imgs\\LiteCSDNet_UPE")
RUAS = os.listdir(".\\mAP\\imgs\\RUAS")
SLiteCSDNet_UPE = os.listdir(".\\mAP\\imgs\\SLiteCSDNet_UPE")
LiteCSDNet_LOL = os.listdir(".\\mAP\\imgs\\LiteCSDNet_LOL")
Zero_DCE_plus = os.listdir(".\\mAP\\imgs\\Zero-DCE++")

for f in sorted(glob.glob(".\\ExDark_all\\*")):
    
    name = f.split("\\")[-1]
 
    temp_imgs = []
    temp_models = []
        
    if os.path.exists(".\\mAP\\imgs\\CSDNet_UPE\\{}".format(name)):

        temp_imgs.append(name)
        temp_models.append("CSDNet_UPE")
        
    if os.path.exists(".\\mAP\\imgs\\LiteCSDNet_UPE\\{}".format(name)):
        
        temp_imgs.append(name)
        temp_models.append("LiteCSDNet_UPE")
        
    if os.path.exists(".\\mAP\\imgs\\RUAS\\{}".format(name)):

        temp_imgs.append(name)
        temp_models.append("RUAS")
        
    if os.path.exists(".\\mAP\\imgs\\SLiteCSDNet_UPE\\{}".format(name)):

        temp_imgs.append(name)
        temp_models.append("SLiteCSDNet_UPE")
        
    if os.path.exists(".\\mAP\\imgs\\LiteCSDNet_LOL\\{}".format(name)):

        temp_imgs.append(name)
        temp_models.append("LiteCSDNet_LOL")

    if os.path.exists(".\\mAP\\imgs\\Zero-DCE++\\{}".format(name)):

        temp_imgs.append(name)
        temp_models.append("Zero-DCE++")

    if len(temp_imgs) >= 2 :
        for m in temp_models:
            shutil.move(".\\mAP\\imgs\\{}\\{}".format(m, name), 
                        ".\\mAP\\imgs_dup\\{}\\{}".format(m, name))

    temp_models = []
    temp_imgs = []
    
os.rename(".\\mAP\\imgs", ".\\mAP\\imgs_unique")
winsound.Beep(freq, duration)

# =============================================================================
# Count of unique images in each model:
# =============================================================================

df_count = pd.DataFrame(columns=["Model", "Count"] )

for folder in glob.glob(".\\mAP\\imgs_unique\\*"):
    folder_name = folder.split("\\")[-1]
    count = len(os.listdir(".\\mAP\\imgs_unique\\{}".format(folder_name)))
    
    append = [folder_name, count]
    df_length = len(df_count)
    df_count.loc[df_length] = append

df_count.to_csv(".\\mAP\\df_count.csv", index=False)


# =============================================================================
# Check the difference between duplicates imgs in terms of higher mAP
# =============================================================================

'''
Remember this part of code will only handle duplicates after
filtering out the unique first, which means at the end you need 
to move the images from img_dup_greater to imgs_unique
'''

os.mkdir(".\\mAP\\imgs_dup_greater\\RUAS")
os.mkdir(".\\mAP\\imgs_dup_greater\\Zero-DCE++")

for f in sorted(glob.glob(".\\ExDark_all\\*")):
     
    name = f.split("\\")[-1]
 
    temp_imgs = []
    temp_models = []
        
        
    if os.path.exists(".\\mAP\\imgs_dup\\RUAS\\{}".format(name)):

        temp_imgs.append(name)
        temp_models.append("RUAS")
        

    if os.path.exists(".\\mAP\\imgs_dup\\Zero-DCE++\\{}".format(name)):

        temp_imgs.append(name)
        temp_models.append("Zero-DCE++")
       
#### Check mAP for dup     
    df = pd.DataFrame(columns=["model_name", "img_name", "mAP"])
    if len(temp_imgs) >= 2 :
        for m in temp_models:
            df_temp = pd.read_csv(".\\mAP\\{}.csv".format(m)) 
            for i, r in df_temp.iterrows():
                if name == r[0]:  # r[0] is the file name  
                
                    append = [m, r[0] , r[1]] # r[1] is the mAP value
                    df_length = len(df)
                    df.loc[df_length] = append
            
        r0_model = df.loc[0][0]
        r0_mAP   = df.loc[0][2]

        r1_model = df.loc[1][0]
        r1_mAP   = df.loc[1][2]
                
        
        if (r0_mAP >= 0.9) and (r1_mAP >= 0.9):
        
            
            if r0_mAP > r1_mAP:
                shutil.copy(".\\ExDark_all\\{}".format(df.loc[0][1]), # image name
                    ".\\mAP\\imgs_dup_greater\\{}\\{}".format(r0_model, df.loc[0][1]))
                print("Zero-DCE++ won!")
                
            elif r1_mAP > r0_mAP:
                shutil.copy(".\\ExDark_all\\{}".format(df.loc[0][1]), # image name
                    ".\\mAP\\imgs_dup_greater\\{}\\{}".format(r1_model, df.loc[0][1]))
                print("RUAS won!")
            else:
                print("Both mAPs are equal")
    
        
    temp_models = []
    temp_imgs = []
    
