import numpy as np 
import matplotlib.pyplot as plt
import glob
import cv2
import os
import seaborn as sns
import pandas as pd
from skimage.filters import sobel
import time 
import pickle
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize
from sklearn import metrics
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.utils import resample

## Included image extensions:
exe = ['.jpg', '.jpeg', '.png', '.PNG', '.JPEG', '.JPG']

## Resize images:
SIZE = 128

# Capture images and labels into arrays. Start by creating empty lists.
train_images = []
train_labels = [] 
images_names = []

#=============================================================================
# FEATURE EXTRACTOR "function"
# =============================================================================
def feature_extractor(dataset):
    x_train = dataset
    current_image = 0
    image_dataset = pd.DataFrame()
    for image in range(x_train.shape[0]): 
        df = pd.DataFrame() 
        input_img = x_train[image, :,:,:]
        img = input_img
        # =============================================================================
        #          FEATURE 1:  Pixel values
        # =============================================================================
        pixel_values = img.reshape(-1)
        df['Pixel_Value'] = pixel_values
        # =============================================================================
        #         FEATURE 2: Gabor Filter
        # =============================================================================
        num = 1 
        kernels = []
        for theta in range(2):   
            theta = theta / 4. * np.pi
            for sigma in (1, 3):  
                lamda = np.pi/4
                gamma = 0.5
                gabor_label = 'Gabor' + str(num)  
                ksize=9
                kernel = cv2.getGaborKernel((ksize, ksize), sigma, theta, lamda, gamma, 0, ktype=cv2.CV_32F)    
                kernels.append(kernel)
                fimg = cv2.filter2D(img, cv2.CV_8UC3, kernel)
                filtered_img = fimg.reshape(-1)
                df[gabor_label] = filtered_img  
                num += 1  

        # =============================================================================
        #         FEATURE 3 : Sobel Filter
        # =============================================================================
        edge_sobel = sobel(img)
        edge_sobel1 = edge_sobel.reshape(-1)
        df['Sobel'] = edge_sobel1
        
        
        image_dataset = image_dataset.append(df)
        current_image = current_image + 1
        print("\n Current Image Features Extracted: {} \n".format(current_image))
            
    return image_dataset


for directory_path in glob.glob("<path_to_folders>\\*"):
# The path must conatins folders with uniqe images for each enhancement models as well as bright (normal) images captured during the daytime.

    print(directory_path.split("\\")[-1], ":",  len(os.listdir(directory_path)))
    label = directory_path.split("\\")[-1]
    for img_path in glob.glob(directory_path + "\\*"): 
        img_name = img_path.split("\\")[-1]
        img = cv2.imread(img_path, cv2.IMREAD_COLOR) #Reading color images
        img = cv2.resize(img, (SIZE, SIZE)) #Resize images
        train_images.append(img)
        train_labels.append(label)
        images_names.append(img_name)
        
print("\n Total number of images in the train_set: {} \n".format(len(train_images)))

## Add labels and names to dataframe:
data = pd.DataFrame()
data["Labels"] = train_labels
data["Names"] = images_names

## Encoding (the labels only) 
le = preprocessing.LabelEncoder()   
le.fit(data["Labels"])
train_labels_encoded = le.transform(data["Labels"])

## Convert images to np.array:
train_images = np.array(train_images)
train_labels_encoded = np.array(train_labels_encoded)

x_train = train_images/255.0

# =============================================================================
# Extract features
# =============================================================================

s = time.time()
image_features = feature_extractor(x_train)
e = time.time()
print("\n Total time taken to extract features from {} TRAIN: {} min".format(x_train.shape[0], (e - s)/60))
n_features = image_features.shape[1] # to get the number of features
image_features = np.expand_dims(image_features, axis=0)  # add a dim of (1) at the beggining 
X_for_RF = np.reshape(image_features, (x_train.shape[0], -1))  #Reshape to images, features

# =============================================================================
# Unsample the minority class
# =============================================================================

df = pd.DataFrame(X_for_RF)
df["labels"] = train_labels_encoded

df["labels"].value_counts()

df_maj =   df[df["labels"] == 0]
df_min_1 = df[df["labels"] == 1]
df_min_2 = df[df["labels"] == 2]

df_min1_upsamled = resample(df_min_1, replace=True, n_samples=200, random_state=42)
df_min2_upsamled = resample(df_min_2, replace=True, n_samples=200, random_state=42)

df_upsampled = pd.concat([df_min1_upsamled, df_min2_upsamled, df_maj])
df_upsampled["labels"].value_counts()

x_sam = df_upsampled.drop(labels=["labels"], axis=1)
y_sam = df_upsampled["labels"].values

## Split the data into train and test data 
x_train_sam, x_test_sam, y_train_sam, y_test_sam = train_test_split(x_sam, y_sam, test_size=0.1, random_state=20)

# =============================================================================
# Random forest Hyper-parameters with GridSearchCV After UpSampling
# =============================================================================

n_estimators = [int(x) for x in np.linspace(start = 10, stop = 80, num = 10)]

param_grid = {'n_estimators': n_estimators} 

model =  RandomForestClassifier()

grid = GridSearchCV(estimator=model,  param_grid=param_grid, cv=3, verbose=2, n_jobs=4)

## Train the RG with GridSearch: 
hs = time.time()
grid.fit(x_train_sam,y_train_sam)
he = time.time()
print("\n Total time for RF hyperparamter: {} sec or {} min \n".format((he - hs), (he - hs)/60))

## What the best paramters:
print('Best hyperparameters:', grid.best_params_)
print('Best score:', grid.best_score_)
print('Print all results:', grid.cv_results_)

n_estimators = [params['n_estimators'] for params in grid.cv_results_['params']]
mean_test_score = grid.cv_results_['mean_test_score']
std_test_score = grid.cv_results_['std_test_score']
params_summary = grid.cv_results_["params"]

## Plot mean test score vs n_estimators:
plt.errorbar(n_estimators, mean_test_score, yerr=std_test_score)
plt.title('Mean test score vs n_estimators')
plt.xlabel('n_estimators')
plt.ylabel('Mean test score')
plt.savefig(".\\classification\\figures\\mean_score_test_vs_n_estimators.png")

## Initialize Random Forest after hyperparameter 
model = RandomForestClassifier(n_estimators = 48,  random_state= 42)

## Fit the model on training data
s3 = time.time()
model.fit(x_train_sam, y_train_sam) #For sklearn no one hot encoding
e3 = time.time()
print("\n Total train RF: {} sec or {} min \n".format((e3 - s3), (e3 - s3)/60))

## Predict:
s4 = time.time()
test_prediction = model.predict(x_test_sam)
e4 = time.time()
print("\n Total time taken to predict on TEST: {} sec \n".format(e4 - s4))

## Inverse le transform to get original labels back (categorical) for both actual and predicted. 
test_prediction = le.inverse_transform(test_prediction)
y_test_sam = le.inverse_transform(y_test_sam)
y_train_sam = le.inverse_transform(y_train_sam)

## Print overall accuracy
print ("Accuracy = {:.2f}%".format((metrics.accuracy_score(y_test_sam, test_prediction)) * 100))

## Unique labels
(unique, counts) = np.unique(test_prediction, return_counts=True)
print(unique, counts)

## ROC AUC
y_test_prob = model.predict_proba(x_test_sam)
print("ROC-AUC score for balanced data is:")
print(roc_auc_score(y_test_sam, y_test_prob , multi_class='ovr', labels=["Bright", "RUAS", "Zero-DCE++"]))

## Train after up-sampling
model = OneVsRestClassifier(model).fit(x_train_sam, y_train_sam)
 
y_test_sam_bin = label_binarize(y_test_sam, classes=np.unique(y_test_sam))
n_classes = len(np.unique(y_test_sam))
 
fpr = dict()
tpr = dict()
roc_auc = dict()
for i in range(n_classes):
    fpr[i], tpr[i], _ = roc_curve(y_test_sam_bin[:, i], y_test_prob[:, i])
    roc_auc[i] = auc(fpr[i], tpr[i])

## Plot ROC curve for each class
plt.figure()
colors = ['blue', 'red', 'green']  # Choose a color for each class
for i, color in zip(range(n_classes), colors):
    plt.plot(fpr[i], tpr[i], color=color, lw=2, label='ROC curve of class ''{}'' (area = {:.2f})'.format(i, roc_auc[i]))
    
plt.plot([0, 1], [0, 1], 'k--', lw=2)  # Plot diagonal line for reference
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC curve for multi-class classification')
plt.legend(loc="lower right")
plt.savefig(".\\classification\\figures\\ROC_curve.png")

'''
## A reminder of binray classes and their category ones ## 
class = 0 is bright
class = 1 RUAS
class = 2 Zero-DCE++
'''

## Confusion Matrix
cm = confusion_matrix(y_test_sam, test_prediction)
sns.heatmap(cm, annot=True,  fmt='g', xticklabels=['0', '1', '2'], yticklabels=['0', '1', '2'])
plt.xlabel('Predicted label')
plt.ylabel('True label')
plt.title('Confusion Matrix')
plt.savefig(".\\classification\\figures\\cm.png")

## Save the trained model as pickle string to disk for future use
model_name = "classifier.pkl"
pickle.dump(model, open(".\\classification\\model\\" + model_name, 'wb'))

## Save split in case this one is the best
# Note that this command will save the features with a size of 1 GB.
np.savez(".\\classification\\results\\my_split.npz".format(num), 
         X_train=x_train_sam, X_test=x_test_sam, y_train=y_train_sam, y_test=y_test_sam)







































































