## Import needed libraries:
import numpy as np 
import matplotlib.pyplot as plt
import glob
import cv2
import pandas as pd
from skimage.filters import sobel
import time 
import pickle
import os

## Resize imgs:
SIZE = 128

## Target image extensions:
exe = ['.jpg', '.jpeg', '.png', '.PNG', '.JPEG', '.JPG']

## Dic for labels: 
dic = {0: "Bright", 1:"RUAS", 2: "Zero-DCE++"}

## Load the model:
with open("./model/classifier10.pkl", 'rb') as file:
    model = pickle.load(file)

## Extract features function:
def feature_extractor(dataset):
    x_train = dataset
    current_img = 0
    img_dataset = pd.DataFrame()
    for img in range(x_train.shape[0]): 
        df = pd.DataFrame() 
        input_img = x_train[img, :,:,:]
        img = input_img
        
        ## Feature 1: Pixel values
        pixel_values = img.reshape(-1)
        df['Pixel_Value'] = pixel_values
        
        ## Feature 2:  Gabor Banks
        num = 1 
        kernels = []
        for theta in range(2):   
            theta = theta / 4. * np.pi
            for sigma in (1, 3):  
                lamda = np.pi/4
                gamma = 0.5
                gabor_label = 'Gabor' + str(num)  
                ksize=9
                kernel = cv2.getGaborKernel((ksize, ksize), sigma, theta, lamda, gamma, 0, ktype=cv2.CV_32F)    
                kernels.append(kernel)
                fimg = cv2.filter2D(img, cv2.CV_8UC3, kernel)
                filtered_img = fimg.reshape(-1)
                df[gabor_label] = filtered_img  
                num += 1  

        ## Feature 3: Sobel Filter
        edge_sobel = sobel(img)
        edge_sobel1 = edge_sobel.reshape(-1)
        df['Sobel'] = edge_sobel1
        img_dataset = pd.concat([img_dataset, df], ignore_index=True)
        current_img = current_img + 1

            
    return img_dataset

## Initialize the dataframe to collect images and their predicted labels:
df = pd.DataFrame(columns=["img_name", "label_predicted"])

# Initialize counter:
c = 1

# Path to input images and print number of images inside:
path = "./test_imgs/*{}".format(exe)
print("Number of imgs: ", len(glob.glob(path)))   

for img_path in sorted(glob.glob(path)):
        img_name = img_path.split("/")[-1]
        test_imgs = []
        img = cv2.imread(img_path, cv2.IMREAD_COLOR) 
        resized = cv2.resize(img, (SIZE, SIZE)) 
        test_imgs.append(resized)
        test_imgs = np.array(test_imgs)
        x_test = test_imgs/255.0
    
        ## Extract features:
        s = time.time()
        img_features = feature_extractor(x_test)
        n_features = img_features.shape[1] 
        img_features = np.expand_dims(img_features, axis=0)  
        X_for_RF = np.reshape(img_features, (x_test.shape[0], -1))  
    
        ## Predict:
        test_prediction = model.predict(X_for_RF)
        e = time.time()
        label = dic[test_prediction[0]]
        
        ## Plot image: 
        fig = plt.figure() 
        ax = fig.add_axes([0, 0, 1, 1])
        ax.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        ax.axis('off')
        plt.show()
        
        ## Print meta-data (time of processing one img, current counter and output label):
        print("############ [{}] ###########".format(c))
        print("#Total time taken to extract and predict: {} is [{:.2f} sec]".format(img_name, e - s))
        print("#The prediction for this img is: [{}] \n".format(label)) 
    
        ## Append to the dataframe created:
        to_append = [img_name, label] 
        df_length = len(df)
        df.loc[df_length] = to_append
        
        ## In case the label is " ", then save the image in the appropiate folder:
        if label == "Zero-DCE++":
            cv2.imwrite("./results/zero/{}".format(img_name), img)
            c = c + 1
        elif label == "RUAS":
            cv2.imwrite(".n/results/ruas/{}".format(img_name), img)
            c = c + 1
        else:
            cv2.imwrite("./results/bright/{}".format(img_name), img)
            c = c + 1

## Save dataframe as a csv file:           
df.to_csv("./results/df.csv", index=False)
            
