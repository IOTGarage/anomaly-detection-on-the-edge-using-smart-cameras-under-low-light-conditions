import torch
import torch.nn as nn
import torchvision
import torch.backends.cudnn as cudnn
import torch.optim
import os
import sys
import argparse
import time
import dataloader
import model
import numpy as np
from torchvision import transforms
from PIL import Image
import glob
import time
import csv


with torch.no_grad():
    
    for file_path in sorted(glob.glob("/scratch/c.c1866386/stages/classification/results/zero/*")):
        image_path = file_path
                    
        name = image_path.split("/")[-1]
        os.environ['CUDA_VISIBLE_DEVICES']='0'
        scale_factor = 12
        data_lowlight1 = Image.open(image_path)
        Height = data_lowlight1.size[1]
        Width  = data_lowlight1.size[0]

        data_lowlight = (np.asarray(data_lowlight1)/255.0)


        data_lowlight = torch.from_numpy(data_lowlight).float()
        
        h=(data_lowlight.shape[0]//scale_factor)*scale_factor
        w=(data_lowlight.shape[1]//scale_factor)*scale_factor
    
        data_lowlight = data_lowlight[0:h,0:w,:]
        data_lowlight = data_lowlight.permute(2,0,1)
        data_lowlight = data_lowlight.cuda().unsqueeze(0)
        
        if data_lowlight.shape[1] == 4 : # since the Conv layer accept 3 channels not 4 
            data_lowlight = data_lowlight.narrow(1, 0, 3)

        DCE_net = model.enhance_net_nopool(scale_factor).cuda()
        DCE_net.load_state_dict(torch.load('./model/Epoch99.pth'))
        start = time.time()
        enhanced_image,params_maps = DCE_net(data_lowlight)
        
        sec = (time.time() - start)

        result_path = "./data/results/{}".format(name) 

        torchvision.utils.save_image(enhanced_image, result_path)
        
        time_inference = sec
        to_append = [name, time_inference, Height, Width]
        print(to_append)
        with open(r'report.csv', 'a') as f:
            writer = csv.writer(f)
            writer.writerow(to_append)


