import torch
import torch.nn as nn
import torchvision
import torch.backends.cudnn as cudnn
import torch.optim
import os
import sys
import argparse
import time
import dataloader
import model
import numpy as np
from torchvision import transforms
from PIL import Image
import glob
import time
import csv


def lowlight(image_path):
	name = image_path.split("/")[-1]
	os.environ['CUDA_VISIBLE_DEVICES']='0'
	scale_factor = 12
	data_lowlight1 = Image.open(image_path)
	#bic = Image.BICUBIC    
	#data_lowlight = data_lowlight.resize((512, 512), bic)
	Height = data_lowlight1.size[1]
	Width  = data_lowlight1.size[0]
	#print(image_path)
 

	data_lowlight = (np.asarray(data_lowlight1)/255.0)


	data_lowlight = torch.from_numpy(data_lowlight).float()

	if data_lowlight.ndim != 3:
		data_lowlight1.save("/home/c.c1866386/gpu_test/3.7/Zero-DCE++/Zero-DCE++/data/results/discarded/{}".format(name)) 
		pass
	

	h=(data_lowlight.shape[0]//scale_factor)*scale_factor
	w=(data_lowlight.shape[1]//scale_factor)*scale_factor
	data_lowlight = data_lowlight[0:h,0:w,:]
	data_lowlight = data_lowlight.permute(2,0,1)
	data_lowlight = data_lowlight.cuda().unsqueeze(0)
	#print(data_lowlight.size())
	#data_lowlight = data_lowlight[:3]
	#print(x.size())
	DCE_net = model.enhance_net_nopool(scale_factor).cuda()
	DCE_net.load_state_dict(torch.load('./snapshots_Zero_DCE++/Epoch99.pth'))
	start = time.time()
	enhanced_image,params_maps = DCE_net(data_lowlight)
	
	sec = (time.time() - start)

	#image_path = image_path.replace('test_data','results')
	result_path = "/home/c.c1866386/gpu_test/3.7/Zero-DCE++/Zero-DCE++/data/results/{}".format(name) 

	#result_path = image_path
	#if not os.path.exists(image_path.replace('/'+image_path.split("/")[-1],'')):
		#os.makedirs(image_path.replace('/'+image_path.split("/")[-1],''))
	# import pdb;pdb.set_trace()
	torchvision.utils.save_image(enhanced_image, result_path)
	
	time_inference = sec
	to_append = [name, time_inference, Height, Width]
	print(to_append)
	with open(r'report.csv', 'a') as f:
		writer = csv.writer(f)
		writer.writerow(to_append)

if __name__ == '__main__':

	with torch.no_grad():

		# starting_file = '2015_00001.png' 
		# directory = "/home/c.c1866386/gpu_test/ExDark_all/"
		# # Get the index of the starting file in the list
		# all_files = sorted(glob.glob(directory + "*"))
		# starting_index = all_files.index(directory + starting_file)

		# Loop over the files starting from the starting index
		#for file_path in all_files[starting_index:]:
		filePath = './data/test_data/*'
		#filePath = "/home/c.c1866386/gpu_test/ExDark_all/*"
		for file_path in sorted(glob.glob(filePath)):
			lowlight(file_path)


		#for image in sorted(glob.glob(filePath)):  # by me
			#lowlight(image) # pass the apth of each img # by me


		# filePath = 'data/test_data/'
		# file_list = os.listdir(filePath)
		# sum_time = 0
		# for file_name in file_list:
		# 	test_list = glob.glob(filePath+file_name+"/*")
		#     for image in test_list:
		#
		# 		print(image)
		# 		sum_time = sum_time + lowlight(image)
		#
		# print(sum_time)

		# file_list = os.listdir(filePath)
		# for file_name in file_list:
		# test_list = glob.glob(filePath+file_name+"/*")
		# for image in test_list: