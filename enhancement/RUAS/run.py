import sys
import time
import numpy as np
import torch
import argparse
import torch.utils
import torch.backends.cudnn as cudnn
from PIL import Image
from torch.autograd import Variable
from model import Network
from multi_read_data import MemoryFriendlyLoader
import pandas as pd
import glob


df = pd.DataFrame(columns=["Image", "Time Inference (in sec)", "Img_H", "Img_W"])

parser = argparse.ArgumentParser("ruas")
# Input to images >> "path_to_ruas":
parser.add_argument('--data_path', type=str, default="/scratch/c.c1866386/stages/classification/results/ruas", help='location of the data corpus')

# Results are saved here:
parser.add_argument('--save_path', type=str, default='./outputs', help='location of the data corpus')

parser.add_argument('--model', type=str, default='upe', help='location of the data corpus')
# Use GPU for processing:
parser.add_argument('--gpu', type=int, default=0, help='gpu device id')
parser.add_argument('--seed', type=int, default=2, help='random seed')

args = parser.parse_args()
save_path = args.save_path
test_low_data_names = args.data_path + '/*'


TestDataset = MemoryFriendlyLoader(img_dir=test_low_data_names, task='test')
test_queue = torch.utils.data.DataLoader(TestDataset, batch_size=1, pin_memory=True, num_workers=0)


def save_images(tensor, path):
    image_numpy = tensor[0].cpu().float().numpy()
    image_numpy = (np.transpose(image_numpy, (1, 2, 0)))
    im = Image.fromarray(np.clip(image_numpy * 255.0, 0, 255.0).astype('uint8'))
    im.save(path, 'png')


def main():
    if not torch.cuda.is_available():
        print('no gpu device available')
        sys.exit(1)

    np.random.seed(args.seed)
    torch.cuda.set_device(args.gpu)
    cudnn.benchmark = True
    torch.manual_seed(args.seed)
    cudnn.enabled = True
    torch.cuda.manual_seed(args.seed)
    print('gpu device = %d' % args.gpu)
    print("args = %s", args)

    model = Network()
    model = model.cuda()
    model_dict = torch.load('./ckpt/'+args.model+'.pt')
    model.load_state_dict(model_dict)


    for p in model.parameters():
        p.requires_grad = False

    with torch.no_grad():

        for _, (input, image_name) in enumerate(test_queue):
            input = Variable(input, volatile=True).cuda()
            image_name = image_name[0].split('/')[-1]
            starttime = time.time()
            u_list, r_list = model(input)
            endtime = time.time()
            sec = endtime - starttime
            u_path = save_path + '/' + image_name
            print('processing {} in {} seconds, size ({},{})'.format(image_name, sec, input.shape[2], input.shape[3]))
            name = image_name
            time_inference = sec
            Height = input.shape[2]
            Width = input.shape[3]
            # Append values to the corresponding columns:
            to_append = [name, time_inference, Height, Width]
            df_length = len(df)
            df.loc[df_length] = to_append

            #print('processing {}'.format(u_name))
            if args.model == 'lol':
                save_images(u_list[-1], u_path)
            elif args.model == 'upe' or args.model == 'dark':
                save_images(u_list[-2], u_path)

    df.to_csv (r'report.csv', index = False, header=True)

if __name__ == '__main__':
    main()
