# Anomaly Detection on the Edge using Smart Cameras under Low-Light Conditions

**Note** that this research was proposed to work on resource-constrained devices (e.g. Raspberry Pi and Jetson Nano Developer Kit). Therefore, replicating the exact research will need additional effort and resources. However, the provided codes allow the implementation of the proposed design separately on a Cloud-based infrastructure. The system consists of three main stages: Classification, Enhancement and Detection (refer to the paper for more details: <http://paper_url_here>). In addition, you can find a guide video (step-by-step) to implement all stages at:https://drive.google.com/file/d/1bSgCf4Ai5nAXrdzPLfuvsozE7EDPw2g8/view?usp=share_link.

<!--Clone Repo-->
<h1>
  Pre-stages
</h1>


1. Clone the repository:
```python 
git clone https://gitlab.com/IOTGarage/anomaly-detection-on-the-edge-using-smart-cameras-under-low-light-conditions.git 
```
2. Rename the repo to _"stages"_ **[Optional]**:
```python 
mv anomaly-detection-on-the-edge-using-smart-cameras-under-low-light-conditions stages
```
3. Check the availability of anaconda and gpu:
```python
conda
nvidia-smi
```

<!-- CLASSIFICATION -->

<h1>
  <img src="/icons/class.png" width="35" height="35" title="Just an Icon">
  Classification
</h1>

In this part, input images with different light intensities (e.g. bright and dark) are first passed through a classifier in order to classify the image into three labels: bright (E0), dark (E1) and dark (E2). (**Note** that darks images might have different intensities, features...etc).

* `my_classifier.py:` This is the main file to classify images.

* `train_classifier.py:` This code allows users to re-train the classifier on their custom dataset.<br>
The **UpSampling** method is used since the number of unique images from the ExDark dataset for _"Zero-DCE++"_ and _"RUAS"_ were lower than bright ones. In addition, **GridSeacrhCV** for Hyper-parameters was applied to find the optimal _"n_estimatots"_ for the random forest. (**Note** that in case of balanced dataset, Upsampling is not necessary). 

* `requierments.txt:` The libraries needed to run the classifier.

* `model:` The folder contains the classifier weights. 

* `results:` This directory contains 3-folders:

  * **zero:** Output folder for low-light images with the label Zero-DCE++.

  * **ruas:** Output folder for low-light images with the label RUAS.

  * **bright:** Output folder for bright images.

   In addition, it will contain a CSV file after running `my_classifier.py`, creating a Dataframe with all images processed with the      predicted label.

* `figures:` This folder contains metric results for the optimal random forest classifier. 

* `test_imgs:` Sample images form the test_set data.

* <img src="/icons/python.png" width="25" height="25" title="icon"> Python >= 3.7 (This task can work on any enviornment and operating system, since most of the packages are created with the creation of any baisc python environment).

* The link to the dataset used for training the classifier can be found in `dataset.txt`.

### Steps:

1. Create a conda environment:
```python
conda create -n classify
```
2. Activate the created "classify":
```python
source activate classify  # In Linux OS
```
3. Upgrade pip **[Optional]**:
```python
pip install --upgrade pip
```
4. Install the libraries in requierments.txt:
```python
cd classification
```
then,
```python 
pip install -r requierments.txt
```
5. Run `my_classifier.py`:
```python
python my_classifier.py
```
<!-- ENHANCEMENT -->

<h1>
  <img src="/icons/brightness.png" width="40" height="40" title="Just an Icon">
  Enhancement
</h1>

Following the classifier, the outputs are passed to two low-light image enhancements to improve the visual features before detection occurs. On the other hand, images with the label _"E0"_, which are considered images captured with sufficient light, directly go through the detection phase (no enhancement required). **Note** that only two enhancement methods were chosen in this work. However, any outperform method can take place in the proposed ones. For more information about the selection strategy, refer to the paper.

## RUAS

* <img src="/icons/python.png" width="25" height="25" title="icon"> Python 3.6.

* <img src="/icons/anaconda.png" width="25" height="25" title="icon"> Anaconda 2019.03.

* <img src="/icons/linux.png" width="25" height="25" title="icon"> Linux x86_64. 

* GPU was used:

  * Tesla P100-PCIE-16GB.
  * CUDA Version: 11.<br>
  (**Note:** Any **NVIDIA GPU** <img src="/icons/nvidia.png" width="25" height="25" title="icon"> with CUDA > 9, can do the job).

`run.py:` This is the main file to run the enhancment model.

`requierments.txt:` This file contains all the pacakges needed to run the _"RUAS"_ model.

`inputs:` Input images located at the directory "./classification/results/ruas/". 

`outputs:` where the enhanced images are saved "./enhancement/RUAS/outputs".

### Steps:

1. Create a conda environment:
```python
conda create -n ruas python=3.6
```
2. Activate the created "ruas":
```python
source activate ruas  # In Linux OS
```
3. Upgrade pip **[Optional]**:
```python
pip install --upgrade pip
```
4. Install the libraries in requierments.txt:
```python
cd enhancement/RUAS
```
then, 
```python 
pip install -r requierments.txt
```
5. Install torch and torchvision:
```python
conda install pytorch=0.4.1 torchvision=0.2.1  -c pytorch
```
6. Run the `run.py`:
```python
python run.py 

```
## Zero-DCE++

* <img src="/icons/python.png" width="25" height="25" title="icon"> Python 3.7.

* <img src="/icons/anaconda.png" width="25" height="25" title="icon"> Anaconda 2019.03.

* <img src="/icons/linux.png" width="25" height="25" title="icon"> Linux x86_64. 

* GPU was used:

  * Tesla P100-PCIE-16GB.
  * CUDA Version: 11.<br>
  (**Note:** Any **NVIDIA GPU** <img src="/icons/nvidia.png" width="25" height="25" title="icon"> with CUDA > 9, can do the job).

`run.py:` This is the main file to run the enhancment model.

`requierments.yml:` This file contains all the pacakges needed to run the _"Zero-DCE++"_ model in a **conda** environment. 

`inputs:` Input images located at the directory "./classification/results/zero/". 

`results:` where the enhanced images are saved "./enhancement/Zero-DCE++/data/results".

### Steps:

1. Create a conda environment:
```python
conda create -n zero python=3.7
```
2. Activate the created "zero":
```python
source activate zero  # In Linux OS
```
3. Upgrade pip **[Optional]**:
```python
pip install --upgrade pip
```
4. Install the libraries in requierments.txt:
```python
cd enhancement/Zero-DCE++
```
then, 
```python 
pip install -r requierments.txt
```
5. Install torch and torchvision:
```python
pip install torch==1.9.0 torchvision==0.10.0 torchaudio==0.9.0
```
6. Run the `run.py`:
```python
python run.py 
```

<!-- DETECTION -->

<h1>
  <img src="/icons/detection.png" width="46" height="46" title="Just an Icon">
  Detection
</h1>

In this final stage, whether enhancement is required or not, all images are passed to the object detection models to identify objects within an image by drawing a bounding box around the desired object. **Note** that in this research, only classes: Person and Vehicle (e.g. car, bus and truck & motorcycle) were considered. Moreover, only the object detection used for evaluation and testing on the Cloud-based is proposed. However, a lightweight object detector was also selected for the resource-constrained devices, (refer to the paper for more information). 
 
* <img src="/icons/python.png" width="25" height="25" title="icon"> Python 3.8.

* <img src="/icons/pytorch.png" width="25" height="25" title="icon"> torch==1.10 & torchvision==0.11.0.

* <img src="/icons/anaconda.png" width="25" height="25" title="icon"> Anaconda 2019.03.

* <img src="/icons/linux.png" width="25" height="25" title="icon"> Linux x86_64. 

* GPU was used:

  * Tesla P100-PCIE-16GB.
  * CUDA Version: 11.<br>
  (**Note:** Any **NVIDIA GPU** <img src="/icons/nvidia.png" width="25" height="25" title="icon"> with CUDA > 9, can do the job).

### Steps:

The below steps are necessary to install and run **Detectron2**:

1. Create a conda environment:
```python
conda create -n detectron2 python=3.8
```

2. Activate the created detectron2:
```python
source activate detectron2  # In Linux OS
```

3. Upgrade pip **[Optional]**:
```python
pip install --upgrade pip
```

4. Move to the _"detection"_ directory:
```python
cd detection
```

5. Clone the **detectron2** repo:
```python
git clone https://github.com/facebookresearch/detectron2.git
```

6. Install torch==1.10 & torchvision==0.11.0 (See the official website):
```python
conda install pytorch==1.10.0 torchvision==0.11.0 torchaudio==0.10.0 cudatoolkit=11.3 -c pytorch -c conda-forge
```

then,
```python
python -m pip install -e detectron2
```

7. Install **Cython**:
```python
pip install cython
```

8. Install **OpenCV**:
```python
pip install opencv-python==4.3.0.38
```

9. Copy `OD.py` (located at: "./detection" in the repo) to "./detectron2".

10. The chosen model is: `faster_rcnn_X_101_32x8d_FPN_3x.yaml` (located at "./detectron2/detectron2/config/COCO-Detection").

11. Create new directories for models, images and output (**Note**: you must be at the detection directory):
```python
mkdir detectron2/models
mkdir detectron2/images
mkdir detectron2/outputs
```

12. Copy the weights `model_final_68b088.pkl` (located at: "./detection/models" in the repo) to 
"./detectron2/detectron2/models".

13. `images:` Move your input images here.

14. `outputs:` Results are saved here, including images with bounding box and text files containing coordinates for each image processed in the "anno" folder.

15. `OD.py:` This is the main code for the object detection task. **Note** that the code was modified to detect only specific classes, as mentioned before. In addition, it generates predictions text files, which were used for the evaluation stage before and after enhancement. 

16. `OD_all_classes.py:` same as `OD.py` but detect other classes than people and vehicles, specifically the ExDark dataset's classes:
{"person", "car", "truck", "bus", "motorcycle","bicycle","boat", "bottle","cat", "chair", "cup", "dog", "table"}.

17. To run the file `OD.py`:

```python
python detectron2/OD.py
```
<!-- EVALUATION -->

<h1>
  <img src="/icons/evaluation.png" width="40" height="40" title="Just an Icon">
  Evaluation
</h1>

**Note** that this section is the data analysis part, which only describes the evaluation steps in practice and does not have to do with the implementation of the above stages.

This part explains the evaluation stage for the low-light image enhacement models along with the object detection. In addition, the steps accomplished to get the unique images per enhancement model for the classification task. <!--**Note that this section is the data analysis part, which only describes the evaluation steps in practice and does not have to do with running any stage (classifier/enhancer/detector)**. -->

## 1. Enhancement & Detection

* The ExDark dataset was passed through each enhancement model + the object detetcion.

* The object detection will generate the predictions in a text file saved at "./anno".

For a fast and easy object detection evaluation, an **object detection metrics** tool was used. (you can find the source in the references section.) to run the tool:

1. Clone the repo:
```python
git clone https://github.com/rafaelpadilla/review_object_detection_metrics.git
```
2. For the python-enviornment, you can use one of the above used before for the classification or enhancement or detection, since it   needs common and base packages and you can install the missed one using `pip install <pacakge-name>`.

3. To run the user interface:
```python
python ./run.py
```
4. This window will appear:
<img src="/extra/odm.png" width="1000" height="800" title="user interface">

**Note** You must put all the assets (e.g. images/gt/anno...etc) in a same directory.

What paths do you need to add?

**Ground Truth**

- `Annotations:` Path to original annotations of the images (gt).
- `Images:` Path to images, you have the choice to choose images before or after enhancement (Check the image shape before and after).
- `Classes:` Path to the class names, you can find it at "./extra/coco.names".
- `Coordinates format:` Since coordinates were converted to YOLO, choose `YOLO(.txt)`.

**Detections**

- `Annotations:` Path to predictions produced by the object detection (anno).
- `Classes:` Path to the class names, you can find it at "./extra/coco.names" (same as before).
- `Coordinates format:` Since coordinates were converted to YOLO, then choose:
  `<class_id> <confidence> <x_center> <y_center> <width> <height> (RELATIVE)`.

**Mertrics**

Any selected metric (ticked), it will be included in the results. **Note** that for this reserach, IOU >= 0.5 was considered.

**Output**

Specify the output directory, to save the results by clicking on **Run**.

* `dataset.txt`: Contains the link share for the entire **ExDark** images and **gt** groud truth (converted). 

## 2. Classification

After passing the entire dataset through each enhancement model, the following steps were implemented to obtain unique outperform images per model with the object detection task using the code located at `./extra/mAP.py` . (**Note** that these steps were implemented for all classes of the Exdark dataset.):

1. The calculation of mAP for a single image per model. By taking the prediction and ground truth files per image and calculating the mAP. (Code Lines:<ins> _"Calculate mAP for all images"_ </ins>). 

  This step was accomplished by:

  * Matching the prediction and ground truth files.
  * Obtain the height and width of the enhanced image.
  * Match the image name in the enhanced and detection folders. 
  * Save results in a CSV dataframe per model for further analysis.
    
2. Read the CSV files and move images with mAP >= threshold to new directories. (Code Lines: <ins> _"Read CSV files and move images with mAP > threshold"_ </ins>). 

3. Since some enhancement models might have common or similar image output, duplicate ones were moved to a new directory by keeping the remaining ones as _"unique images"_. (Code Lines: <ins>_"Check similar images in the images directory and move them to separate ones "_</ins>).

  This step was accomplished by:
  
  * Creating a new directory for duplicate images per enhancement model.
  * Move the duplicates to the new directories.
  * Rename the folders with the remaining images as _"unique_images"_.

4. Count the unique images per enhancement model. (Code: <ins>_"Count of unique images in each model"_</ins>).
  
5. Since the threshold was set as 0.9, some duplicate images in all models might have different accuracy. (For instance, 0.93 and 0.95 on image_1 for Zero-DCE++ and RUAS, respectively. Therefore this image can be moved to RUAS unique folder and so on.). Moreover, this step was done only for the chosen models in this research, Zero-DCE++ and RUAS. (Code Lines: <ins> _"Check the difference between duplicate images in terms of higher mAP "_</ins>). 

## Notes

* Images with label _"Bright"_ or _"E0"_ go directly to the detection phase.
* For the detection task, `OD.py` _"input_path"_ must be changed to the folders (bright, zero & ruas) in the classification directory or you need to move them manually. 
* Same GPU and CUDA version work properly for all tasks.

## References

* Detectron2 Documentation: https://detectron2.readthedocs.io/en/latest/tutorials/install.html.
* PyTorch official website: https://pytorch.org/.
* The ExDark Dataset: https://github.com/cs-chan/Exclusively-Dark-Image-Dataset.
* Object Detection Metrics: https://github.com/rafaelpadilla/review_object_detection_metrics.

## BibTeX 

If you plan to use any information or findings from my research in your work or publications, could you please cite my research to acknowledge the source of information properly. 

Thank you for considering this request 😊

```python 
citation_here
```
